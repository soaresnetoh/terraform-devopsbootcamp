variable "aws_region" {
  default = "us-east-1"
}

variable "aws_tags" {
  type = map(string)
  default = {
    env         = "dev"
    product     = "devops-bootcamp-ping"
    cost-center = "IT"
    hashtag     = "doutor-estranho"
  }
}

variable "aws_s3_bucket_name" {
  default = "meutestehernani-bucket"
}