resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.aws_s3_bucket_name

  tags = var.aws_tags
}

variable "environment" {
  default = "dev_us-east-1"
}