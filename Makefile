TF_PLAN_FILE="tf-plan"
ENVIRONMENT="dev_us-east-1"

config:
	rm -rf ${HOME}/.tfenv && \
	git clone https://github.com/tfutils/tfenv.git ~/.tfenv && \
    export PATH="${HOME}/.tfenv/bin:${PATH}" && \
    tfenv install


init:
	rm -rf .terraform && \
	terraform init -backend-config=backend_${ENVIRONMENT}.tfvars -reconfigure && \
	terraform fmt -recursive && \
	terraform validate

plan:
	chmod -R 777 .terraform/*
	terraform plan -out ${TF_PLAN_FILE}

apply:
	chmod -R +x .terraform/ && \
	terraform apply --auto-approve ${TF_PLAN_FILE}

destroy:
	terraform destroy --auto-approve